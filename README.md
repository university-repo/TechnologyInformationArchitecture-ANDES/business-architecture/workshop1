# Nutri-Health Business Architecture

This document describes the business architecture for Nutri-Health, a company aimed at selling healthy food based on nutritional preferences, dietary restrictions, and health objectives. Nutri-Health offers a convenient option for delicious and nutritious meals for individuals looking to improve their dietary habits without leaving their homes.

## Objective

The objective of this document is to design and specify a comprehensive business plan for Nutri-Health, developing models from business, financial, strategic, and capability perspectives as described in the business architect readings by Professor Jorge Villalobos from the Universidad de los Andes.

## Delivery

Each group is required to submit a PDF file via BNe containing the complete document and an Excel file detailing the financial model projected over 5 years. The complete document should not exceed 50 pages.

## Evaluation

Evaluation will be based on a rubric published in BNe, assigning 70% to the content of the submission and 30% based on the group's ranking compared to others in the course. The highest score achievable is 5.0, descending without ties.

## Content

The document should include the following 4 chapters:

### Chapter 1 – Business Model

- Description of Nutri-Health's business model, including value proposition, customer segments, distribution channels, and cost structure.

### Chapter 2 – Financial Model

- Detailed 5-year financial model for Nutri-Health, covering revenues, expenses, investments, and profitability analysis.

### Chapter 3 – Strategic Model

- Strategic vision for Nutri-Health, long-term objectives, market analysis, and growth strategies.

### Chapter 4 – Business Capability Model

- Description of key capabilities for Nutri-Health, including technology, operations, and human resources.

## Business Idea

Nutri-Health is an initiative by the New Business Unit of the Grupo Empresarial de los Alpes. It focuses on a food business with expansion into multiple cities in Colombia (at least 9 cities by the end of year 5), featuring a strong digital component and an innovative approach focused on efficiency and sustainability.

## Budget

A budget of three million dollars is allocated for investment in the development and launch of Nutri-Health.

## Restrictions

Nutri-Health must meet specific characteristics outlined and should not resemble businesses from previous projects or be used in other courses.

## Information and Assumptions

Reasonable assumptions are permitted for business development, provided they are documented and credible.

## Members

- Juan Nicolas Silva
- Erik Ferney Cubillos
- Johan David Mora
- Andrés Felipe Wilches Torres
